package ru.ilinovsg.tm;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IllegalAccessException {
        List<Object> list = new ArrayList<>();
        list.add(new Person("Alex", "Ivanov", LocalDate.now(), "alex_i@gmail.com"));
        list.add(new Person("Ben","ben_p@gmail.com"));
        ReflectionUtils.getClassFields(list);

    }
}
