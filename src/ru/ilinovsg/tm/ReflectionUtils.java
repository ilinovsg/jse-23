package ru.ilinovsg.tm;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class ReflectionUtils {
    private ReflectionUtils() {
    }

    public static List<List<Description>> getClassFields(List<Object> objects) {
        List<List<Description>> result = new ArrayList<>();
        for(Object object: objects) {
            Class<?> clazz = object.getClass();
            if (clazz.getTypeName() != Person.class.getTypeName()) {
                throw new IllegalArgumentException();
            }
            result.add(fieldIterator(clazz.getDeclaredFields(), object));

            if (clazz.getSuperclass() != null) {
                result.add(fieldIterator(clazz.getSuperclass().getDeclaredFields(), object));
            }
        }
        return result;
    }

    private static List<Description> fieldIterator(Field[] fields, Object object) {
        List<Description> list = new ArrayList<>();
        for (Field field : fields){
            boolean hasValue = false;
            try {
                field.setAccessible(true);
                if (field.get(object) != null) {hasValue = true;}
            } catch (IllegalAccessException e) {
            }
            list.add(new Description(field.getName(), field.getType().getTypeName(), hasValue));
        }
        return list;
    }
}
